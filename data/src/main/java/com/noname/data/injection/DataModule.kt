package com.noname.data.injection

import org.kodein.di.Kodein

/**
 * <p>
 * Copyright (c) 2020, Babel Sistemas de Información. All rights reserved.
 * </p>
 *
 * @author <a href=“mailto:alvaro.montero@babel.es”>Alvaro Montero</a>
 *
 * Date: 2020-02-16
 */

fun generateDataModule() = Kodein.Module(name = "DataModule") {

}
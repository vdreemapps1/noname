package com.noname.presentation.state

import com.noname.presentation.model.BackModel
import com.noname.presentation.model.TabbarModel
import com.noname.presentation.model.ToolbarModel
import es.babel.easymvvm.core.state.EmaBaseState


data class HomeToolbarsState(
    val tabbarModel: TabbarModel = TabbarModel(),
    val toolbarModel: ToolbarModel = ToolbarModel(),
    val backModel: BackModel = BackModel()
) : EmaBaseState
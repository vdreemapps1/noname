package com.noname.presentation.base

import com.noname.presentation.ui.MainToolbarsViewModel
import es.babel.easymvvm.core.navigator.EmaNavigationState
import es.babel.easymvvm.core.state.EmaBaseState
import org.kodein.di.generic.instance

/**
 * <p>
 * Copyright (c) 2020, Babel Sistemas de Información. All rights reserved.
 * </p>
 *
 * @author <a href=“mailto:alvaro.montero@babel.es”>Alvaro Montero</a>
 *
 * Date: 2020-02-18
 */

abstract class BaseToolbarsFragment<S : EmaBaseState, VM : BaseToolbarsViewModel<S, NS>, NS : EmaNavigationState> :
    BaseFragment<S, VM, NS>() {

    lateinit var mainToolbarsVm: MainToolbarsViewModel

    private val mainToolbarsViewModelSeed: MainToolbarsViewModel by instance()

    override fun onInitialized(viewModel: VM) {

        (viewModel as? BaseToolbarsViewModel<*, *>)?.also {
            mainToolbarsVm = addExtraViewModel(mainToolbarsViewModelSeed, this, requireActivity())
            it.mainToolbarsVm = mainToolbarsVm
            onInitializedWithToolbarsManagement(viewModel, mainToolbarsVm)
        } ?: throw RuntimeException("The view model must be inherited from BaseToolbarsViewModel")
    }

    abstract fun onInitializedWithToolbarsManagement(
        viewModel: VM,
        mainToolbarViewModel: MainToolbarsViewModel
    )
}
package com.noname.presentation.base

import com.noname.presentation.ui.MainToolbarsViewModel
import es.babel.easymvvm.core.navigator.EmaNavigationState
import es.babel.easymvvm.core.state.EmaState


/**
 * <p>
 * Copyright (c) 2020, Babel Sistemas de Información. All rights reserved.
 * </p>
 *
 * @author <a href=“mailto:alvaro.montero@babel.es”>Alvaro Montero</a>
 *
 * Date: 2020-02-18
 */

abstract class BaseToolbarsViewModel<T, NS : EmaNavigationState> : BaseViewModel<T, NS>() {

    lateinit var mainToolbarsVm: MainToolbarsViewModel

    override fun onStart(inputState: EmaState<T>?): Boolean {
        val firstTime = super.onStart(inputState)
        onConfigureToolbars(mainToolbarsVm)
        return firstTime
    }

    abstract fun onConfigureToolbars(mainToolbarsVm: MainToolbarsViewModel)
}
package com.noname.presentation.base

import es.babel.easymvvm.android.viewmodel.EmaViewModel
import es.babel.easymvvm.core.navigator.EmaNavigationState
import es.babel.easymvvm.core.state.EmaState

abstract class BaseViewModel<T, NS : EmaNavigationState> : EmaViewModel<T, NS>() {

    override fun onStart(inputState: EmaState<T>?): Boolean {
        val firsTime = super.onStart(inputState)
        onStartAnalytic()
        return firsTime
    }

    override fun onStartFirstTime(statePreloaded: Boolean) {
    }


    abstract fun onStartAnalytic()
}
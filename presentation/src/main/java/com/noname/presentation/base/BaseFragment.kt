package com.noname.presentation.base

import android.content.Context
import android.view.inputmethod.InputMethodManager
import com.noname.presentation.KODEIN_TAG_DIALOG_SIMPLE
import com.noname.presentation.injection.generateFragmentModule
import es.babel.easymvvm.android.ui.EmaFragment
import es.babel.easymvvm.core.dialog.EmaDialogProvider
import es.babel.easymvvm.core.navigator.EmaNavigationState
import es.babel.easymvvm.core.state.EmaBaseState
import es.babel.easymvvm.core.state.EmaExtraData
import org.kodein.di.Kodein
import org.kodein.di.generic.instance
import java.util.logging.Logger

abstract class BaseFragment<S : EmaBaseState, VM : BaseViewModel<S, NS>, NS : EmaNavigationState> :
    EmaFragment<S, VM, NS>() {

    override fun injectFragmentModule(kodein: Kodein.MainBuilder): Kodein.Module =
        generateFragmentModule(this)

    override val fragmentViewModelScope: Boolean
        get() = true

    private val dialogProvider: EmaDialogProvider by instance(KODEIN_TAG_DIALOG_SIMPLE)

    protected val logger: Logger by instance()

    private var lastTimeClicked: Long = 0

    override fun onStateError(error: Throwable) {
    }

    override fun onStateLoading(data: EmaExtraData) {
        onLoading(data)
    }

    override fun onStateNormal(data: S) {
        dialogProvider.hide()
        onNormal(data)
    }

    /**
     * We create this abstract function if we want to appy default behaviours in [onStateNormal],[onStateError],[onStateLoading]
     * @param data
     */
    abstract fun onLoading(data: EmaExtraData)

    abstract fun onNormal(data: S)

    abstract fun onError(error: Throwable): Boolean

    override fun onNavigation(navigation: EmaNavigationState) {
        super.onNavigation(navigation)
        context?.let {
            val hideKeyboard: InputMethodManager =
                it.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            view?.let { view ->
                hideKeyboard.hideSoftInputFromWindow(view.windowToken, 0)
            }
        }
    }

}


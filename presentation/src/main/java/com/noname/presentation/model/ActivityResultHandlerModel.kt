package com.noname.presentation.model


import com.noname.domain.INT_NEGATIVE
import com.noname.domain.INT_ZERO


data class ActivityResultHandlerModel(
        val id: Int,
        val implementation: ((Int, Result, Any?) -> Boolean) //RETURN TRUE IF IT IS REMOVE AFTER CONSUMED
) {
    sealed class Result(val code: Int) {

        object Success : Result(INT_NEGATIVE)

        object Fail : Result(INT_ZERO)

        class Other(code: Int) : Result(code)
    }
}
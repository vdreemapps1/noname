package com.noname.presentation.model

import com.noname.domain.STRING_EMPTY

data class ToolbarModel(
    val closeSessionVisibility: Boolean = true,
    val title: String = STRING_EMPTY,
    val visibility: Boolean = true,
    val gone: Boolean = true
)
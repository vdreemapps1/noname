package com.noname.presentation.model

data class BackModel(
    val disabled: Boolean = false,
    val implementation: (() -> Unit)? = null
)
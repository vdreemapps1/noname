package com.noname.presentation.model

import com.noname.domain.INT_NEGATIVE
import com.noname.domain.STRING_EMPTY
import java.io.Serializable

data class TabbarModel(
    val expanded: Boolean = false,
    val visibility: Boolean = true,
    val buttons: List<TabbarButtonModel> = emptyList(), /*Buttons starting from left to right, no center button included*/
    val expandedTexts: List<TabbarTextExpandedModel> = emptyList()
) : Serializable

data class TabbarButtonModel(
    val text: String = STRING_EMPTY,
    val icon: Int = INT_NEGATIVE,
    val clickListener: ((Boolean) -> Unit)? = null,
    val selected: Boolean = false
) : Serializable

data class TabbarTextExpandedModel(
    val text: String = STRING_EMPTY,
    val clickListener: (() -> Unit)? = null
) : Serializable
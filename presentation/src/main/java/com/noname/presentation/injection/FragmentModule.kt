package com.noname.presentation.injection

import androidx.fragment.app.Fragment
import com.noname.presentation.ui.home.HomeViewModel
import com.noname.presentation.ui.splash.SplashViewModel
import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.provider
import org.kodein.di.generic.singleton

/**
 * <p>
 * Copyright (c) 2020, Babel Sistemas de Información. All rights reserved.
 * </p>
 *
 * @author <a href=“mailto:alvaro.montero@babel.es”>Alvaro Montero</a>
 *
 * Date: 2020-02-16
 */

fun generateFragmentModule(fragment: Fragment) = Kodein.Module(name = "FragmentModule") {

    bind<Fragment>() with provider { fragment }

    bind<SplashViewModel>() with singleton { SplashViewModel() }

    bind<HomeViewModel>() with singleton { HomeViewModel() }

}

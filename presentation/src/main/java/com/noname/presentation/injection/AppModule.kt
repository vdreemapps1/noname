package com.noname.presentation.injection

import android.app.Application
import org.kodein.di.Kodein

/**
 * <p>
 * Copyright (c) 2020, Babel Sistemas de Información. All rights reserved.
 * </p>
 *
 * @author <a href=“mailto:alvaro.montero@babel.es”>Alvaro Montero</a>
 *
 * Date: 2020-02-16
 */


fun generateAppModule(app: Application) = Kodein.Module(name = "AppModule") {


}

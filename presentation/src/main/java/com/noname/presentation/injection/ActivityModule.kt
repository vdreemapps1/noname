package com.noname.presentation.injection

import androidx.fragment.app.FragmentManager
import androidx.navigation.NavController
import com.noname.presentation.KODEIN_TAG_DIALOG_SIMPLE
import com.noname.presentation.base.BaseActivity
import com.noname.presentation.dialog.simple.simple.SimpleDialogProvider
import com.noname.presentation.ui.MainToolbarsViewModel
import com.noname.presentation.ui.splash.SplashNavigator
import es.babel.easymvvm.core.dialog.EmaDialogProvider
import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.provider
import org.kodein.di.generic.singleton

fun injectionActivityModule(activity: BaseActivity) = Kodein.Module(name = "ActivityModule") {

    bind<FragmentManager>() with provider { activity.supportFragmentManager }

    bind<NavController>() with singleton { activity.navController }

    bind<MainToolbarsViewModel>() with provider { MainToolbarsViewModel() }

    bind<EmaDialogProvider>(tag = KODEIN_TAG_DIALOG_SIMPLE) with provider {
        SimpleDialogProvider(instance())
    }

    bind<SplashNavigator>() with singleton { SplashNavigator(instance()) }

}

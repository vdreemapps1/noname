package com.noname.presentation.ui.home

import androidx.navigation.NavController
import com.noname.presentation.base.BaseNavigator
import es.babel.easymvvm.core.navigator.EmaNavigationState

class HomeNavigator(override val navController: NavController) :
    BaseNavigator<HomeNavigator.Navigation>() {

    sealed class Navigation : EmaNavigationState
}
package com.noname.presentation.ui.splash

import com.noname.presentation.base.BaseViewModel
import kotlinx.coroutines.delay


class SplashViewModel :
    BaseViewModel<SplashState, SplashNavigator.Navigation>() {
    override fun onStartAnalytic() {
    }


    override fun createInitialViewState(): SplashState {
        return SplashState()
    }

    override fun onStartFirstTime(statePreloaded: Boolean) {
        super.onStartFirstTime(statePreloaded)
        executeUseCase {
            delay(3000L)
            navigate(SplashNavigator.Navigation.SplashToHome)
        }
    }
}
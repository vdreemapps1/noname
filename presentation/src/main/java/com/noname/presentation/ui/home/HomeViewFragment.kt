package com.noname.presentation.ui.home

import com.noname.presentation.R
import com.noname.presentation.base.BaseToolbarsFragment
import com.noname.presentation.ui.MainToolbarsViewModel
import es.babel.easymvvm.core.navigator.EmaBaseNavigator
import es.babel.easymvvm.core.state.EmaExtraData
import org.kodein.di.generic.instance

/**
 * <p>
 * Copyright (c) 2020, Babel Sistemas de Información. All rights reserved.
 * </p>
 *
 * @author <a href=“mailto:alvaro.montero@babel.es”>Alvaro Montero</a>
 *
 * Date: 2020-02-18
 */

class HomeViewFragment :
    BaseToolbarsFragment<HomeState, HomeViewModel, HomeNavigator.Navigation>() {
    override fun onInitializedWithToolbarsManagement(
        viewModel: HomeViewModel,
        mainToolbarViewModel: MainToolbarsViewModel
    ) {

    }

    override fun onLoading(data: EmaExtraData) {
    }

    override fun onNormal(data: HomeState) {
    }

    override fun onError(error: Throwable): Boolean {
        return false
    }

    override val inputStateKey: String? = null

    override val navigator: EmaBaseNavigator<HomeNavigator.Navigation>? by instance()
    override val viewModelSeed: HomeViewModel by instance()

    override fun getFragmentLayout(): Int = R.layout.fragment_home

    override fun onSingleEvent(data: EmaExtraData) {
    }

}
package com.noname.presentation.ui

import android.os.Bundle
import android.view.View
import androidx.constraintlayout.widget.ConstraintLayout
import com.noname.domain.STRING_EMPTY
import com.noname.presentation.KODEIN_TAG_DIALOG_SIMPLE
import com.noname.presentation.R
import com.noname.presentation.base.BaseActivity
import com.noname.presentation.model.ActivityResultHandlerModel
import com.noname.presentation.model.BackModel
import com.noname.presentation.model.TabbarModel
import com.noname.presentation.model.ToolbarModel
import com.noname.presentation.ui.home.HomeNavigator
import com.noname.presentation.state.HomeToolbarsState
import es.babel.easymvvm.android.ui.EmaView
import es.babel.easymvvm.core.dialog.EmaDialogProvider
import es.babel.easymvvm.core.state.EmaExtraData
import kotlinx.android.synthetic.main.activity_base.emaAppBarLayout
import kotlinx.android.synthetic.main.activity_base.navHostFragment
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.toolbar.*
import org.kodein.di.generic.instance


class MainToolbarsViewActivity : BaseActivity(),
    EmaView<HomeToolbarsState, MainToolbarsViewModel, HomeNavigator.Navigation> {

    override val viewModelSeed: MainToolbarsViewModel by instance()

    override val navigator: HomeNavigator by instance()

    override val inputState: HomeToolbarsState? = null

    private var bottomViewMargin: Int = 0

    private var backModel: BackModel? = null

    private val closeSessionDialog: EmaDialogProvider by instance(tag = KODEIN_TAG_DIALOG_SIMPLE)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initializeViewModel(this)
        emaAppBarLayout.elevation = 0f
        bottomViewMargin =
            (navHostFragment.view?.layoutParams as? ConstraintLayout.LayoutParams)?.bottomMargin
                ?: 0
    }

    private var vm: MainToolbarsViewModel? = null

    override fun onViewModelInitalized(viewModel: MainToolbarsViewModel) {
        vm = viewModel
        setupToolbar(viewModel)
    }

    private fun setupToolbar(viewModel: MainToolbarsViewModel) {
        navController.addOnDestinationChangedListener { _, destination, _ ->
            //TODO uncomment this line when home fragment created
            //val backVisibility = destination.id != R.id.homeDashboardViewFragment
            viewModel.onActionUpdateToolbar(false) {
                it.copy(
                    //backVisibility = backVisibility,
                    title = destination.label?.toString() ?: STRING_EMPTY
                )
            }
        }
        clToolbarBack.setOnClickListener { viewModel.onActionBackClicked() }
        ivToolbarCloseSession.setOnClickListener { viewModel.onActionCloseSessionClicked() }
    }

    override fun onBackPressed() {
        backModel?.let {
            if (!it.disabled) {
                checkBackImplementation()
            }
        } ?: onBackSystemPressed()

    }

    private fun checkBackImplementation() {

        backModel?.implementation?.invoke() ?: onBackSystemPressed()
    }

    override fun onStateNormal(data: HomeToolbarsState) {
        closeSessionDialog.hide()
        if (checkToolbarVisibility(data)) {
            updateToolbar(data.toolbarModel)
        }
        checkTabbarVisibility(data)
        updateTabbar(data.tabbarModel)
        backModel = data.backModel
    }

    private fun updateTabbar(tabbarModel: TabbarModel) {
    }


    private fun checkTabbarVisibility(data: HomeToolbarsState) {
        if (data.tabbarModel.visibility)
            showTabbar()
        else
            hideTabbar()
    }

    private fun checkToolbarVisibility(data: HomeToolbarsState): Boolean {
        if (data.toolbarModel.visibility) showToolbar()
        else hideToolbar(data.toolbarModel.gone)
        return data.toolbarModel.visibility
    }

    private fun updateToolbar(data: ToolbarModel) {
    }


    override fun onStateLoading(data: EmaExtraData) {
    }

    override fun onSingleEvent(data: EmaExtraData) {
        when (data.type) {
            MainToolbarsViewModel.SINGLE_ACTIVITY_RESULT_HANDLER_ADD -> {
                addActivityResultHandler(data.extraData as ActivityResultHandlerModel)
            }
            MainToolbarsViewModel.SINGLE_ACTIVITY_RESULT_HANDLER_REMOVE -> {
                removeActivityResultHandler(data.extraData as Int)
            }
            else -> {
            }
        }
    }

    override fun onStateError(error: Throwable) {

    }

    private fun onBackSystemPressed() {
        vm?.onActionBackClicked()
    }

    override fun getToolbarTitle(): String? = STRING_EMPTY

    override fun getNavGraph(): Int = R.navigation.navigation_main

    override fun getLayout(): Int = R.layout.activity_main

    private fun showTabbar() {
        (navHostFragment.view?.layoutParams as? ConstraintLayout.LayoutParams)?.bottomMargin =
            bottomViewMargin
        clTabbar.visibility = View.VISIBLE
    }

    private fun hideTabbar() {
        (navHostFragment.view?.layoutParams as? ConstraintLayout.LayoutParams)?.bottomMargin = 0
        clTabbar.visibility = View.GONE
    }
}
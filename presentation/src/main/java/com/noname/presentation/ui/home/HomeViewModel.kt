package com.noname.presentation.ui.home

import com.noname.presentation.base.BaseToolbarsViewModel
import com.noname.presentation.ui.MainToolbarsViewModel

/**
 * <p>
 * Copyright (c) 2020, Babel Sistemas de Información. All rights reserved.
 * </p>
 *
 * @author <a href=“mailto:alvaro.montero@babel.es”>Alvaro Montero</a>
 *
 * Date: 2020-02-19
 */

class HomeViewModel : BaseToolbarsViewModel<HomeState, HomeNavigator.Navigation>() {

    override fun onConfigureToolbars(mainToolbarsVm: MainToolbarsViewModel) {
    }

    override fun onStartAnalytic() {
    }

    override fun createInitialViewState(): HomeState = HomeState()

}
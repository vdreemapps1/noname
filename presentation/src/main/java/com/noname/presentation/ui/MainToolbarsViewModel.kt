package com.noname.presentation.ui

import com.noname.domain.FLOAT_ZERO
import com.noname.presentation.base.BaseViewModel
import com.noname.presentation.model.ActivityResultHandlerModel
import com.noname.presentation.model.BackModel
import com.noname.presentation.model.TabbarModel
import com.noname.presentation.model.ToolbarModel
import com.noname.presentation.ui.home.HomeNavigator
import com.noname.presentation.state.HomeToolbarsState
import es.babel.easymvvm.core.state.EmaExtraData


class MainToolbarsViewModel : BaseViewModel<HomeToolbarsState, HomeNavigator.Navigation>() {

    companion object {
        const val SINGLE_ACTIVITY_RESULT_HANDLER_ADD = 1
        const val SINGLE_ACTIVITY_RESULT_HANDLER_REMOVE = 2
    }

    override fun onStartAnalytic() {
    }

    override fun createInitialViewState(): HomeToolbarsState {
        return HomeToolbarsState()
    }

    fun onActionBackClicked() {
//        navigate(HomeNavigator.Navigation.Back)
    }

    fun onActionShowToolbar(show: Boolean, gone: Boolean = true) {
        updateViewState {
            copy(toolbarModel = toolbarModel.copy(visibility = show, gone = gone))
        }
    }

    fun onActionUpdateToolbar(
        update: Boolean = true,
        updateToolbar: (ToolbarModel) -> ToolbarModel
    ) {
        checkViewState {
            updateViewState(update) {
                copy(toolbarModel = updateToolbar.invoke(it.toolbarModel))
            }
        }
    }

    fun onActionUpdateTabbar(updateTabbar: ((TabbarModel) -> TabbarModel)? = null) {
        updateTabbar?.also {
            updateViewState {
                val tabbarUpdated = it.invoke(tabbarModel)
                copy(
                    tabbarModel = tabbarUpdated,
                    toolbarModel = toolbarModel.copy(closeSessionVisibility = tabbarUpdated.visibility)
                )
            }
        } ?: updateViewState()
    }

    fun onActionHandleBack(update: (currentBackModel: BackModel) -> BackModel) {
        checkViewState {
            updateViewState {
                copy(backModel = update.invoke(backModel))
            }
        }
    }

    fun onTabbarSlideAction(offset: Float) {
        if (offset <= FLOAT_ZERO)
            updateViewState {
                copy(tabbarModel = tabbarModel.copy(expanded = false))
            }
    }

    fun onActionCloseSessionClicked() {
        loading()
    }


    fun addActivityResultHandler(activityResultHandlerModel: ActivityResultHandlerModel) {
        sendSingleEvent(
            EmaExtraData(
                SINGLE_ACTIVITY_RESULT_HANDLER_ADD,
                activityResultHandlerModel
            )
        )
    }

    fun removeActivityResultHandler(id: Int) {
        sendSingleEvent(EmaExtraData(SINGLE_ACTIVITY_RESULT_HANDLER_REMOVE, id))
    }
}
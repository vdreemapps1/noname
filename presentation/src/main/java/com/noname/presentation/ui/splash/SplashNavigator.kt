package com.noname.presentation.ui.splash

import androidx.navigation.NavController
import com.noname.presentation.R
import com.noname.presentation.base.BaseNavigator
import es.babel.easymvvm.core.navigator.EmaBaseNavigator
import es.babel.easymvvm.core.navigator.EmaNavigationState


class SplashNavigator(override val navController: NavController) :
    BaseNavigator<SplashNavigator.Navigation>() {

    sealed class Navigation : EmaNavigationState {

        object SplashToHome : SplashNavigator.Navigation() {
            override fun navigateWith(navigator: EmaBaseNavigator<out EmaNavigationState>) {
                val nav = navigator as SplashNavigator
                nav.fromSplashToHome()
            }
        }
    }

    private fun fromSplashToHome() {
        navigateWithAction(R.id.action_splashViewFragment_to_mainToolbarsViewActivity)
    }
}
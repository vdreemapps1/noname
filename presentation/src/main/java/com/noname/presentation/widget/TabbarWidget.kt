package com.noname.presentation.widget

import android.content.Context
import android.content.res.TypedArray
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.View
import com.noname.presentation.R
import es.babel.easymvvm.android.ui.EmaBaseLayout

class TabbarWidget : EmaBaseLayout {

    constructor(context: Context) : super(context)
    constructor(ctx: Context, attrs: AttributeSet) : super(ctx, attrs)
    constructor(ctx: Context, attrs: AttributeSet, defStyleAttr: Int) : super(
        ctx,
        attrs,
        defStyleAttr
    )

    private var heightExpand: Int? = null

    override fun setupAttributes(ta: TypedArray) {

    }

    private fun adjustHeight() {
        if (heightExpand == null) {
            requestLayout()
        }

    }

    override fun getAttributes(): IntArray? = null

    override fun setup(mainLayout: View) {
    }

    override fun onInterceptTouchEvent(ev: MotionEvent?): Boolean {
        adjustHeight()
        return false
    }

    override fun getLayout(): Int = R.layout.layout_tabbar

}
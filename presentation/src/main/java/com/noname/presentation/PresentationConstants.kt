package com.noname.presentation

/**
 * <p>
 * Copyright (c) 2020, Babel Sistemas de Información. All rights reserved.
 * </p>
 *
 * @author <a href=“mailto:alvaro.montero@babel.es”>Alvaro Montero</a>
 *
 * Date: 2020-02-16
 */

const val SPLASH_DELAY = 3000L

const val KODEIN_TAG_DIALOG_SIMPLE = "KODEIN_TAG_DIALOG_SIMPLE"
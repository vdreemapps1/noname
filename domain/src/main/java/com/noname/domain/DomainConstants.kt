package com.noname.domain

/**
 * <p>
 * Copyright (c) 2020, Babel Sistemas de Información. All rights reserved.
 * </p>
 *
 * @author <a href=“mailto:alvaro.montero@babel.es”>Alvaro Montero</a>
 *
 * Date: 2020-02-16
 */

const val STRING_EMPTY = ""


const val INT_NEGATIVE = -1
const val INT_ZERO = 0

const val FLOAT_ZERO = 0f
